.data
printformat:
.string "%ld\n"


.text
.global main
main:
	mov	$90,	%rax
	mov	$0,	%rcx	
	mov	$1,	%rdx

loop:
	mov	%rdx,	%rbx
	addq	%rcx,	%rdx
	mov	%rbx,	%rcx

	push	%rax
	push	%rcx
	push	%rdx


	mov	$printformat,	%rdi
	mov	%rcx,	%rsi
	mov	$0,	%rax
	call	printf

	pop	%rdx
	pop	%rcx
	pop	%rax

	dec	%rax	
	jne	loop


	mov	$0,	%rax
	ret
