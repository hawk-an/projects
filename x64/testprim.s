.data

printformat:
.string "%d\n"

curr:
.word	0x0

.text
.global main
main:
	push	%rbx
	mov	%rsp,	%rbx
	mov	$1000000,	%ecx
	mov	$2,	%r8D
	sub	$4,	%rsp
	mov	%r8D,	(%rsp)
	mov	$3,	%r8D
	jmp	save	

isprime:			#not a primenumber
	add	$2,	%r8D
	mov	%rbx,	%rsi

	mov	%r8D,	(curr)

	fild	(curr)
	sub	$4,	%rsp
	fsqrt
	fistp	(%rsp)
	mov	(%rsp),	%edi
	add	$4,	%rsp

loop:				#not done checking primenumber
	sub	$4,	%rsi

	
	cmp	(%rsi),	%edi
	jb	save
	

	mov	$0,	%edx
	mov	%r8D,	%eax
	divl	(%rsi)

	cmp	$0,	%edx
	jz	ret		#remainder on div == 0 -> not a prime


	cmp	%rsi,	%rsp
	jnz	loop


save:
	sub	$4,	%rsp
	mov	%r8D,	(%rsp)	#save primenumber on stack

	jmp	print

ret:
	cmp	%ecx,	%r8D
	jb	isprime
	
	mov	%rbx,	%rsp
	pop	%rbx
	mov	$0,	%rax
	ret



print:
	sub	$4,	%rsp
	mov	%ecx,	(%rsp)

	mov	$printformat,	%rdi
	mov	%r8,	%rsi
	mov	$0,	%rax
	call	printf		#print primenumber
	
	mov	(%rsp),	%ecx
	add	$4,	%rsp
	mov	(%rsp),	%r8D
	jmp	ret


