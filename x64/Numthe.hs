module Numthe where

import Data.List

primes :: [Integer]
primes = 2 : filter ((==1) . length . primeFactors) [3,5..]
 
primeFactors :: Integer -> [Integer]
primeFactors n = factor n primes
    where
        factor _ [] = []
        factor m (p:ps) | p*p > m        = [m]
                        | m `mod` p == 0 = p : factor (m `div` p) (p:ps)
                        | otherwise      = factor m ps
 
isPrime :: Integer -> Bool
isPrime 1 = False
isPrime n = case (primeFactors n) of
                (_:_:_)   -> False
                _         -> True

integerDigits = reverse.map (`mod` 10) . takeWhile (> 0) . iterate (`div` 10) . abs

fromDigits xs = toNum' 1 (reverse xs)
        where   toNum' _ [] = 0
                toNum' n (x:xs) = x * n + toNum' (n*10) xs

factorial n = foldl (*) 1 [1..n]
