module Main where

import Numthe
import Data.List




--main = putStr $ show $ takeWhile (<1000000) primes


b = takeWhile (<200000) primes

main = do
	s <- readFile "prim.txt"
	let a = map read (lines s) :: [Integer]

	putStr $ show (a \\ b)
	
