#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ncurses.h>
#include "vector/vec.h"
#include "vector/mat.h"


#define PI_OVER_360 0.00872664625997164788
#define PI_OVER_180 0.0174532925



void drawLine(float* a,float* b);
void swap(float* a,float* b);
void buildPerspProjMat(float *m, float fov, float aspect, float znear, float zfar);
void lookat( float *matrix, float *eye, float *center, float *y );
void project(float *m,float* in,float* out);
void plot(int x, int y);
void multiplyMatrix(float* m1,float* m2);
void multiply(float *m,float* v);
void rotate( float* v3,float* forward, float degree);


int t = 0;
int position = 0;
int w;
int h;
float near = 1;
float far = 10;

float fov = 45;

int main()
{
	int ch = 0;

	float ndc[24*3];
	int npoints = 24*3;
	float points[24*3] = 
	{
		1.0f,1.0f,-1.0f, //back
		1.0f,-1.0f,-1.0f,
		-1.0f,1.0f,-1.0f,
		1.0f,-1.0f,-1.0f,
		-1.0f,1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,
		
		1.0f,1.0f,1.0f, //front
		1.0f,-1.0f,1.0f,
		-1.0f,1.0f,1.0f,
		1.0f,-1.0f,1.0f,
		-1.0f,1.0f,1.0f,
		-1.0f,-1.0f,1.0f,
		
		-1.0f,1.0f,1.0f, //left
		-1.0f,-1.0f,1.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f,1.0f,-1.0f,
		-1.0f,1.0f,1.0f,
		-1.0f,-1.0f,-1.0f,
		1.0f,1.0f,1.0f, //right
		1.0f,-1.0f,1.0f,
		1.0f,-1.0f,-1.0f,
		1.0f,1.0f,-1.0f,
		1.0f,1.0f,1.0f,
		1.0f,-1.0f,-1.0f,
	};
	
	vec3 up = {0.0f,1.0f,0.0f};
	vec3 eye = {0.0f,0.0f, -10.0f};
	vec3 forward = {0.0f,0.0f,1.0f};

	initscr();
	raw();
	keypad(stdscr, TRUE);
	noecho();

	while(ch != 'q')
	{
		float projectionmatrix[16];
		float transformationmatrix[16];		
		vec3 center;

		getmaxyx(stdscr,h,w);
		clear();

		memcpy(center,forward,3*sizeof(float));
		sumVec3(center,eye);
		
		
		lookat( transformationmatrix, eye, center, up );

		int j;
		for(j=0;j<4;j++)
		{
			mvprintw(16+(j),0,"%f %f %f %f\n",transformationmatrix[j], transformationmatrix[j+4],transformationmatrix[j+8],transformationmatrix[j+12]);
		}
	
		buildPerspProjMat( projectionmatrix,fov,w/h,near,far);


		for(j=0;j<4;j++)
		{
			mvprintw(10+(j),0,"%f %f %f %f\n",projectionmatrix[j], projectionmatrix[j+4],projectionmatrix[j+8],projectionmatrix[j+12]);
		}
		
		vec4 cords;
		int i;
		for(i=0;i<npoints;i+=3)
		{
			memcpy(cords,&points[i],3*sizeof(float));
			cords[3] = 1;
			multiply(transformationmatrix,cords);
			multiply(projectionmatrix,cords);	
			homogeneousdivideVec4(&ndc[i],cords);
		}

		for(i=0;i<npoints;i+=9)
		{
			drawLine(&ndc[i],&ndc[i+3]);
			drawLine(&ndc[i+3],&ndc[i+6]);
			drawLine(&ndc[i+6],&ndc[i]);
		}
		
		move(h-1,0);
		refresh();
		ch = getch();
		
		if(ch == 'a')
		{
		
			vec3 side;
			crossProductVec3(side, up, forward);
			scalarMulVec3(side, 0.3f);
			sumVec3(eye, side);
		}
		else if(ch == 'd')
		{
			vec3 side;
			crossProductVec3(side, up, forward);
			scalarMulVec3(side, -0.3f);
			sumVec3(eye, side);
		}
		else if(ch == 's')
		{
			vec3 scalar;
			memcpy(scalar,forward,3*sizeof(float));
			scalarMulVec3(scalar, -0.3f);
			sumVec3(eye, scalar);
		}
		else if(ch == 'w')
		{
			vec3 scalar;
			memcpy(scalar,forward,3*sizeof(float));
			scalarMulVec3(scalar, 0.3f);
			sumVec3(eye, scalar);
		}
		else if(ch == 'r')
		{
			eye[1] -= 0.1f;
		}
		else if(ch == 'f')
		{
			eye[1] += 0.1f;
		}
		else if(ch == KEY_LEFT)
		{
			rotate( up,forward, 0.9f);
		}
		else if(ch == KEY_RIGHT)
		{
			rotate( up,forward, -0.9f);
		}
		else if(ch == KEY_UP)
		{
			vec3 side;
			crossProductVec3(side, up, forward);
			rotate( side,forward, 0.9f);
		}
		else if(ch == KEY_DOWN)
		{
			vec3 side;
			crossProductVec3(side, up, forward);
			rotate( side,forward, -0.9f);
		}
		else if(ch == 't')
		{
			fov += 1;
		}
		else if(ch == 'g')
		{
			fov -= 1;
		}
		
		position = 0;
		t = 0;
	}
	endwin();

	return 0;
}


void plot(int x, int y)
{
	mvprintw(h-y-1,x, "#");
}

void quaternion(vec3 v3, vec4 v4)
{
	normalizeVec3(v3);

	vec4 vecQuat;
	memcpy(vecQuat,v3,3*sizeof(float));
	vecQuat[3] = 0.0f;

	vec4 conj;
	memcpy(conj,v4,4*sizeof(float));

	complexConjugateVec4(conj);
	quaternionProductVec4(vecQuat,conj);

	quaternionProductVec4(v4, vecQuat);

	memcpy(v3,v4,3*sizeof(float));
}

void rotate( vec3 v3,vec3 forward, float degree)
{
	float angle = 0.5f * degree * (float)PI_OVER_180;
	float sinangle = sin(angle);
	vec4 nrot;
	memcpy(nrot,v3,3*sizeof(float));
	normalizeVec3(nrot);
	
	nrot[0] = nrot[0]*sinangle;
	nrot[1] = nrot[1]*sinangle;
	nrot[2] = nrot[2]*sinangle;
	nrot[3] = cos(angle);
	
	quaternion(forward,nrot);
			
}

void multiplyMatrix(float* m1,float* m2)
{
	float result[16];
	
	int i,j;
	for(i = 0; i < 16; i++)
	{
		result[i] = 0;
		int row = i%4;
		int line = i - row;

		for(j = 0; j < 4; j++)
		{
			result[i] += m1[row]*m2[line];
			row +=4;
			line ++;
		}
	}
	memcpy(m1,result,16*sizeof(float));
}


void swap(float* a, float* b)
{
	float c = *a;
	*a = *b;
	*b = c;
}

void multiply(float *m,float* v)
{
	float x = 0,y = 0,z = 0,w = 0;
	
	int i;
	for(i = 0; i < 16; i++)
	{
		int j = i%4;
		
		if(j == 0)
			x += v[i/4]*m[i];
		else if(j == 1)
			y += v[i/4]*m[i];
		else if(j == 2)
			z += v[i/4]*m[i];
		else if(j == 3)
			w += v[i/4]*m[i];
	}

	v[0] = x;
	v[1] = y;
	v[2] = z;
	v[3] = w;
}

void drawLine(float* a, float* b)
{


	float y0 = (a[1] +1 ) * h/2;
	float y1 = (b[1] +1 ) * h/2;
	
	float x0 = (a[0] + 1 ) * w/2;
	float x1 = (b[0] + 1 ) * w/2;

	//return if nan
	if(x0 != x0)
		return;
	if(x1 != x1)
		return;
	if(y0 != y0)
		return;
	if(y1 != y1)
		return;
	
	int steep = abs(y1 - y0) > abs(x1 - x0);
	if(steep)
	{
		swap(&x0, &y0);
		swap(&x1, &y1);
	}
	if(x0 > x1)
	{
		swap(&x0, &x1);
		swap(&y0, &y1);
	}
	float dx = x1 - x0;
	float dy = abs(y1 - y0);
	float error = 0.0f;
	float de = dy/dx;
	int ystep;
	int y = (int) y0;
	int x;
	
	if (y0 < y1)
	{
		ystep = 1;
	}
	else
	{
		ystep = -1;
	}

	for(x = (int) x0; x < (int) x1;x++){
		if (steep)
		{
			plot(y, x);
		}
		else
		{
			plot(x, y);
		}

		error += de;
		
		if(error >= 0.5f)
		{
			y += ystep;
			error -= 1.0f;
		}
	}
}

void buildPerspProjMat(float *m, float fov, float aspect, float znear, float zfar)
{
	const float hb = 1.0f/tan(fov*PI_OVER_360);
	float neg_depth = zfar-znear;

	m[0] = hb / aspect;
	m[1] = 0;
	m[2] = 0;
	m[3] = 0;

	m[4] = 0;
	m[5] = hb;
	m[6] = 0;
	m[7] = 0;

	m[8] = 0;
	m[9] = 0;
	m[10] = (zfar + znear)/neg_depth;
	m[11] = -1;

	m[12] = 0;
	m[13] = 0;
	m[14] = (2.0f*znear*zfar)/neg_depth;
	m[15] = 0;
  

}

void lookat( float *matrix, float *eye, float *center, float *y )
{
	float tmp;	
	vec3 forward, side, up;

	forward[0] = center[0] - eye[0];
	forward[1] = center[1] - eye[1];
	forward[2] = center[2] - eye[2];
	normalizeVec3(forward);

	crossProductVec3(side, y, forward);
	normalizeVec3(side);

	crossProductVec3(up, forward, side);

	dotProductVec3(&tmp,side,eye);
	matrix[0] = side[0];
	matrix[4] = side[1];
	matrix[8] = side[2];
	matrix[12] = -tmp;

	dotProductVec3(&tmp,up,eye);
	matrix[1] = up[0];
	matrix[5] = up[1];
	matrix[9] = up[2];
	matrix[13] = -tmp;
	
	dotProductVec3(&tmp,forward,eye);
	matrix[2] = forward[0];
	matrix[6] = forward[1];
	matrix[10] = forward[2];
	matrix[14] = -tmp;

	matrix[3] = matrix[7] = matrix[11] = 0.0f;
	matrix[15] = 1.0f;
	

}
















