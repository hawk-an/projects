#include "vec.h"



void divVec2(vec2 a,vec2 b)
{
}
void divVec3(vec3 a,vec3 b)
{
}
void divVec4(vec4 a,vec4 b)
{
}

void sumVec2(vec2 a,vec2 b)
{
	a[0] += b[0];
	a[1] += b[1];

}
void sumVec3(vec3 a,vec3 b)
{
	a[0] += b[0];
	a[1] += b[1];
	a[2] += b[2];
}
void sumVec4(vec4 a,vec4 b)
{
	a[0] += b[0];
	a[1] += b[1];
	a[2] += b[2];
	a[3] += b[3];
}

void scalarMulVec2(vec2 a, float scal)
{
	a[0] *= scal;
	a[1] *= scal;
}
void scalarMulVec3(vec3 a, float scal)
{
	a[0] *= scal;
	a[1] *= scal;
	a[2] *= scal;
}
void scalarMulVec4(vec4 a, float scal)
{
	a[0] *= scal;
	a[1] *= scal;
	a[2] *= scal;
	a[3] *= scal;
}

void normalizeVec2(vec2 a)
{
	float l = lengthVec2(a);
	if(l != 1.0f )
		return;

	a[0] /= l;
	a[1] /= l;
}
void normalizeVec3(vec3 a)
{
	float l = lengthVec3(a);
	if(l != 1.0f )
		return;

	a[0] /= l;
	a[1] /= l;
	a[2] /= l;
}
void normalizeVec4(vec4 a)
{
	float l = lengthVec3(a);
	if(l != 1.0f )
		return;

	a[0] /= l;
	a[1] /= l;
	a[2] /= l;
	a[3] /= l;
	
}

void dotProductVec3(float *r,vec3 a, vec3 b)
{
	*r = a[0] * b[0];
	*r += a[1] * b[1];
	*r += a[2] * b[2];
}
void crossProductVec3(vec3 r,vec3 a, vec3 b)
{
	r[0] = a[1] * b[2] - a[2] * b[1];
	r[1] = a[2] * b[0] - a[0] * b[2];
	r[2] = a[0] * b[1] - a[1] * b[0];
}

void homogeneousdivideVec4(vec3 r,vec4 a)
{
	r[0] = a[0] / a[3];
	r[1] = a[1] / a[3];
	r[2] = a[2] / a[3];
}

void quaternionProductVec4(vec4 a, vec4 b)
{
	vec4 tmp;
	memcpy(tmp,a,4*sizeof(float));
	
	a[0] = tmp[3] * b[0] + tmp[0] * b[3] + tmp[1] * b[2] - tmp[2] * b[1];
	a[1] = tmp[3] * b[1] + tmp[1] * b[3] + tmp[2] * b[0] - tmp[0] * b[2];
	a[2] = tmp[3] * b[2] + tmp[2] * b[3] + tmp[0] * b[1] - tmp[1] * b[0];
	a[3] = tmp[3] * b[3] - tmp[0] * b[0] - tmp[1] * b[1] - tmp[2] * b[2];
}

void complexConjugateVec4(vec4 a)
{
	a[0] = -a[0];
	a[1] = -a[1];
	a[2] = -a[2];
	//a[3] = a[3];
}











