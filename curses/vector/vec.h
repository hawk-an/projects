#include <math.h>
#include <string.h>
#include <stdio.h>


typedef	float vec2[2];
typedef	float vec3[3];
typedef	float vec4[4];

#define lengthVec2(a) sqrt(a[0]*a[0] + a[1]*a[1])
#define lengthVec3(a) sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2])
#define lengthVec4(a) sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2] + a[3]*a[3])

void divVec2(vec2 a, vec2 b);
void divVec3(vec3 a, vec3 b);
void divVec4(vec4 a, vec4 b);

void sumVec2(vec2 a, vec2 b);
void sumVec3(vec3 a, vec3 b);
void sumVec4(vec4 a, vec4 b);

void scalarMulVec2(vec2 a, float scal);
void scalarMulVec3(vec3 a, float scal);
void scalarMulVec4(vec4 a, float scal);

void normalizeVec2(vec2 vector);
void normalizeVec3(vec3 vector);
void normalizeVec4(vec4 vector);

void dotProductVec3(float *r, vec3 a, vec3 b);
void crossProductVec3(vec3 r, vec3 a, vec3 b);

void homogeneousdivideVec4(vec3 r, vec4 a);

void quaternionProductVec4(vec4 a, vec4 b);
void complexConjugateVec4(vec4 a);




