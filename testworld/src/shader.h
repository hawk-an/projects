#ifndef SHADER_H_
#define SHADER_H_

#include <sys/select.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <sys/types.h>
#include <math.h>
#include <GL/glew.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "textfile.h"

#define EVENT_SIZE		(sizeof(struct inotify_event))
#define EVENT_BUF_LEN	(1024 * (EVENT_SIZE + 16))

typedef struct Shader shader;

struct Shader{
	GLuint s;
	char *vertfile;
	char *fragfile;
	char *folder;
	int fd,wd;
};

void shader_init(shader *shade, char *folder, char *name);
void shader_printshaderinfo(GLuint obj);
void shader_printprograminfo(GLuint obj);
void shader_load(shader *shade);
void shader_update(shader *shade);

#endif
