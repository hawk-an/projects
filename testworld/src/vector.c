#include"vector.h"

void vector4_init(vector4 *this, vector3 v, float angle)
{
	float sinAngle;
	vector3 vn = v;

	angle *= 0.5f;
	vector3_normalize(&vn);
 
	sinAngle = sin(angle);
 
	this->x = vn.x * sinAngle;
	this->y = vn.y * sinAngle;
	this->z = vn.z * sinAngle;
	this->w = cos(angle);
}

void vector4_getcomplexconjugate(vector4 *this)
{
	this->x *= -1;
	this->y *= -1;
	this->z *= -1;
}

void vector4_quaternionproduct3(vector4 *this, vector3 *v)
{
	vector3 vn = *v;
	vector4 vecq;
	vector4 resq = *this;
	vector4 complx = *this;
	
	vector3_normalize(&vn);
 
	vecq.x = vn.x;
	vecq.y = vn.y;
	vecq.z = vn.z;
	vecq.w = 0.0f;
 
 	vector4_getcomplexconjugate(&complx);
	vector4_quaternionproduct4(&vecq, complx);
	vector4_quaternionproduct4(&resq, vecq);

	v->x = resq.x;
	v->y = resq.y;
	v->z = resq.z;
}

void vector4_quaternionproduct4(vector4 *this, vector4 v)
{
	vector4 tmp = *this;
	//float x = e1, y = e2, z = e3, w = e4;

	this->x = tmp.w * v.x + tmp.x * v.w + tmp.y * v.z - tmp.z * v.y;
	this->y = tmp.w * v.y + tmp.y * v.w + tmp.z * v.x - tmp.x * v.z;
	this->z = tmp.w * v.z + tmp.z * v.w + tmp.x * v.y - tmp.y * v.x;
	this->w = tmp.w * v.w - tmp.x * v.x - tmp.y * v.y - tmp.z * v.z;
}

//this = this +v2
void vector3_sum(vector3 *this, vector3 v2)
{
	this->x += v2.x;
	this->y += v2.y;
	this->z += v2.z;
}

//this = this - v
void vector3_sub(vector3 *this, vector3 v2)
{
	this->x -= v2.x;
	this->y -= v2.y;
	this->z -= v2.z;
}

//this = this x v
void vector3_crossproduct(vector3 *this, vector3 v)
{
	vector3 tmp = *this;
	this->x = tmp.y * v.z - tmp.z * v.y;
	this->y = tmp.z * v.x - tmp.x * v.z;
	this->z = tmp.x * v.y - tmp.y * v.x;
}


void vector3_scalar(vector3 *this, float x)
{
	this->x *= x;
	this->y *= x;
	this->z *= x;
}

void vector3_length(vector3 this, float *x)
{
	*x = sqrt( this.x*this.x + this.y*this.y + this.z*this.z);
}

void vector3_normalize(vector3 *this)
{
	float l;
	vector3_length(*this,&l);

	if(l != 1.0f && l != 0.0f)
	{
		this->x /= l;
		this->y /= l;
		this->z /= l;
	}
}
