#include "matrix.h"

void matrix4_setidentity(matrix4 *mat)
{
	memset(mat->values,0,sizeof(float)*16);
	mat->values[0] = 1;
	mat->values[4+1] = 1;
	mat->values[8+2] = 1;
	mat->values[12+3] = 1;
}


void matrix4_setperspective(matrix4 *mat, float fov, float aspect_ratio, float near, float far)
{
//    float scale = tanf(degtorad(angle * 0.5)) * n;
//    float r = imageAspectRatio * scale, l = -r;
//    float t = scale, b = -t;
//    matrix4_setfrustum(mat,l, r, b, t, n, f);
	float ymax = near * tanf(fov * M_PI / 360.0f);
	float xmax = ymax * aspect_ratio;
	matrix4_setfrustum(mat,-xmax, xmax, -ymax, ymax, near, far);	

}

//left, right, bottom, top, near, far
void matrix4_setfrustum(matrix4 *matrix, float l, float r, float b, float t, float n, float f)
{
	float *mat = matrix->values;

    mat[0] = 2 * n / (r - l);
    mat[1] = 0;
    mat[2] = 0;
    mat[3] = 0;
 
    mat[(1*4)+0] = 0;
    mat[(1*4)+1] = 2 * n / (t - b);
    mat[(1*4)+2] = 0;
    mat[(1*4)+3] = 0;
 
    mat[(2*4)+0] = (r + l) / (r - l);
    mat[(2*4)+1] = (t + b) / (t - b);
    mat[(2*4)+2] = -(f + n) / (f - n);
    mat[(2*4)+3] = -1;
 
    mat[(3*4)+0] = 0;
    mat[(3*4)+1] = 0;
    mat[(3*4)+2] = -2 * f * n / (f - n);
    mat[(3*4)+3] = 0;
}
void matrix4_mulv(matrix4 *mat, float v)
{
	int i;
	for(i = 0; i < 16;i++)
	{
		mat->values[i] *= v;
	}

}

void matrix4_mulmatrix4(matrix4 *mat, matrix4 m1, matrix4 m2)
{
//	float *res = mat->values;
	int i,j,k;

	for(i = 0; i < 4; i++)
	{
		for(j = 0; j < 4;j++)
		{
			mat->values[(i*4)+j] = 0;
			for(k = 0; k < 4; k++)
			{
				mat->values[(i*4)+j] +=
					m1.values[(i*4)+k] * m2.values[(k*4)+j];
			}
		}
	}
}

void matrix4_lookat(matrix4 *mat, point eye, point front, vector3 up)
{
	vector3 s;
	vector3 u;
	matrix4 M;
	matrix4 T;
	vector3 f;

	//F = front - e;
	point_getvector3(&f,front,eye);

	vector3_normalize(&f);
	vector3_normalize(&up);
	
	//s = f x u
	s = f;
	vector3_crossproduct(&s,up);

	vector3_normalize(&s);

	//u = s x f
	u = s;
	vector3_crossproduct(&u,f);

	//set matrixes

	matrix4_setidentity(&M);
	matrix4_setidentity(&T);

	
//	matrix4_print(M);	

	M.values[0] = s.x;
	M.values[4] = s.y;
	M.values[8] = s.z;

	M.values[1] = u.x;
	M.values[5] = u.y;
	M.values[9] = u.z;

	M.values[2] = -f.x;
	M.values[6] = -f.y;
	M.values[10] = -f.z;
	
	T.values[12] = -eye.x;
	T.values[13] = -eye.y;
	T.values[14] = -eye.z;

	//memcpy(mat->values,T.values,sizeof(float)*16);
	matrix4_mulmatrix4(mat, T, M);
}

void matrix4_print(matrix4 mat)
{
	int i,j;
	for(i = 0; i < 4; i++)
	{
		for(j = 0; j < 4; j++)
		{
			printf("%f ", mat.values[(i*4)+j]);

		}
		printf("\n");
	}
	printf("\n");

}

void matrix3_print(matrix3 mat)
{
	int i,j;
	for(i = 0; i < 3; i++)
	{
		for(j = 0; j < 3; j++)
		{
			printf("%f ", mat.values[(i*3)+j]);

		}
		printf("\n");
	}
	printf("\n");

}
void matrix4_transpose(matrix4 *mat)
{
	float tmp[16];
	float *m;
	m = mat->values;

	tmp[0]=m[0]; tmp[1]=m[4]; tmp[2]=m[8]; tmp[3]=m[12];
	tmp[4]=m[1]; tmp[5]=m[5]; tmp[6]=m[9]; tmp[7]=m[13];
	tmp[8]=m[2]; tmp[9]=m[6]; tmp[10]=m[10]; tmp[11]=m[14];
	tmp[12]=m[3]; tmp[13]=m[7]; tmp[14]=m[11]; tmp[15]=m[15];
	

	memcpy(mat->values, tmp, sizeof(float)*16);
	
}
void matrix3_determinant(matrix3 mat, float *result)
{
	float *m = mat.values;
	*result = 
		m[0+0]*m[1+1*3]*m[2+2*3] + 
    	m[1+0]*m[2+1*3]*m[0+2*3] + 
    	m[2+0]*m[0+1*3]*m[1+2*3] - 
    	m[0+0]*m[2+1*3]*m[1+2*3] -
    	m[1+0]*m[0+1*3]*m[2+2*3] - 
    	m[2+0]*m[1+1*3]*m[0+2*3];
}
void matrix4_determinant(matrix4 mat, float *result)
{
	float *m = mat.values;
	*result = 
		m[12] * m[9]  * m[6]  * m[3]   -  m[8] * m[13] * m[6]  * m[3]   -
		m[12] * m[5]  * m[10] * m[3]   +  m[4] * m[13] * m[10] * m[3]   +
		m[8]  * m[5]  * m[14] * m[3]   -  m[4] * m[9]  * m[14] * m[3]   -
		m[12] * m[9]  * m[2]  * m[7]   +  m[8] * m[13] * m[2]  * m[7]   +
		m[12] * m[1]  * m[10] * m[7]   -  m[0] * m[13] * m[10] * m[7]   -
		m[8]  * m[1]  * m[14] * m[7]   +  m[0] * m[9]  * m[14] * m[7]   +
		m[12] * m[5]  * m[2]  * m[11]  -  m[4] * m[13] * m[2]  * m[11]  -
		m[12] * m[1]  * m[6]  * m[11]  +  m[0] * m[13] * m[6]  * m[11]  +
		m[4]  * m[1]  * m[14] * m[11]  -  m[0] * m[5]  * m[14] * m[11]  -
		m[8]  * m[5]  * m[2]  * m[15]  +  m[4] * m[9]  * m[2]  * m[15]  +
		m[8]  * m[1]  * m[6]  * m[15]  -  m[0] * m[9]  * m[6]  * m[15]  -
		m[4]  * m[1]  * m[10] * m[15]  +  m[0] * m[5]  * m[10] * m[15];
}

void matrix4_cofactor(matrix4 *mat)
{
	matrix4 tmp;
	float res;
	int i,j,n;

	for ( n = 0 ; n < 16 ; n++ )
	{

		i = 0;
		matrix3 minor;
		for(j = 0; j < 16; j++)
		{
			//if not same col or row
			if((n%4 != j%4) && (n/4 != j/4))
			{
				minor.values[i++] = mat->values[j];
			}
		}
		//matrix3_print(minor);
		//calculate determinat of minor
		matrix3_determinant(minor, &res);
		tmp.values[n] = pow(-1,n%4 + n/4) *res;
		//output->cols[col].row[row] =  cofactor_ij_v1(source, col, row);
	}
	memcpy(mat->values, tmp.values, sizeof(float)*16);
	//matrix4_print(*mat);

}

void matrix4_inverse(matrix4 *mat)
{
	float det;
	// InvM = (1/det(M)) * Transpose(Cofactor(M))
	matrix4_determinant(*mat,&det);
	matrix4_cofactor(mat);
	matrix4_transpose(mat);
	det = 1/det;
	matrix4_mulv(mat,det);
}
