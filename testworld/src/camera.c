#include"camera.h"


void camera_init(camera *c)
{
	point p = {0.0,0.0,6.0};
	vector3 forward = {0.0,0.0,-1.0};
	vector3 up = {0.0,1.0,0.0};

	c->eye = p;
	c->forward = forward;
	c->up = up;
}

void camera_getright(camera *c, vector3 *right)
{
	*right = c->forward;	
	vector3_crossproduct(right,c->up);
}

void camera_render(camera *c, matrix4 *model)
{
	point front = c->eye;
	point_sumvector3(&front, c->forward);

	matrix4_lookat( model, c->eye,front,c->up);
}

void camera_rotate(camera *c, vector3 v, float degree)
{
	vector4 nrot;
	vector4_init(&nrot, v , degree * PIOVER180);
	vector4_quaternionproduct3(&nrot, &c->forward);
}

void camera_translate(camera *c, vector3 v, float s)
{
	vector3_scalar(&v,s);
	point_sumvector3(&c->eye,v);
}

void camera_update(camera *c)
{
	if(glfwGetKey(GLFW_KEY_DOWN))
	{
		vector3 right;
		camera_getright(c,&right);
		camera_rotate(c, right, -0.9f);
	}
	if(glfwGetKey(GLFW_KEY_UP))
	{
		vector3 right;
		camera_getright(c,&right);
		camera_rotate(c, right, 0.9f);
	}
	if(glfwGetKey(GLFW_KEY_RIGHT))
	{
		camera_rotate(c, c->up, -0.9f);
	}
	if(glfwGetKey(GLFW_KEY_LEFT))
	{
		camera_rotate(c, c->up, 0.9f);
	}	

	if( glfwGetKey( 'W'))
	{
		camera_translate(c, c->forward, 0.5f);
	}
	if( glfwGetKey( 'S'))
	{
		camera_translate(c, c->forward, -0.5f);
	}
	if( glfwGetKey( 'D'))
	{
		vector3 right;
		camera_getright(c,&right);
		camera_translate(c, right, 0.5f);
	}
	if( glfwGetKey( 'A'))
	{
		vector3 right;
		camera_getright(c,&right);
		camera_translate(c, right, -0.5f);
	}

	if(glfwGetKey('E'))
	{
		camera_translate(c,c->up, -0.5f);
	}

	if(glfwGetKey('Q'))
	{
		camera_translate(c,c->up, 0.5f);
	}
}
