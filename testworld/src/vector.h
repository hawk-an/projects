#ifndef VECTOR_H_
#define VECTOR_H_

#include<math.h>

typedef struct Vec3 vector3;
typedef struct Vec4 vector4;

struct Vec3
{
	float x;
	float y;
	float z;
};

struct Vec4
{
	float x;
	float y;
	float z;
	float w;
};

void vector4_init(vector4 *this, vector3 v, float angle);
void vector4_quaternionproduct3(vector4 *this, vector3 *v);
void vector4_quaternionproduct4(vector4 *this, vector4 v);
void vector3_sum(vector3 *this, vector3 v2);
void vector3_sub(vector3 *this, vector3 v2);
void vector3_crossproduct(vector3 *this, vector3 v);
void vector3_scalar(vector3 *this, float x);
void vector3_length(vector3 this, float *x);
void vector3_normalize(vector3 *this);

#endif
