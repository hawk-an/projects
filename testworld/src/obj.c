#include "obj.h"

void obj_generatecube(obj *o, float size)
{
	int i;
	float vert[] =
	{
	    -1.0f,-1.0f,-1.0f,
	    -1.0f,-1.0f, 1.0f,
	    -1.0f, 1.0f, 1.0f,
	    1.0f, 1.0f,-1.0f,
	    -1.0f,-1.0f,-1.0f,
	    -1.0f, 1.0f,-1.0f,
	    1.0f,-1.0f, 1.0f,
	    -1.0f,-1.0f,-1.0f,
	    1.0f,-1.0f,-1.0f,
	    1.0f, 1.0f,-1.0f,
	    1.0f,-1.0f,-1.0f,
	    -1.0f,-1.0f,-1.0f,
	    -1.0f,-1.0f,-1.0f,
	    -1.0f, 1.0f, 1.0f,
	    -1.0f, 1.0f,-1.0f,
	    1.0f,-1.0f, 1.0f,
	    -1.0f,-1.0f, 1.0f,
	    -1.0f,-1.0f,-1.0f,
	    -1.0f, 1.0f, 1.0f,
	    -1.0f,-1.0f, 1.0f,
	    1.0f,-1.0f, 1.0f,
	    1.0f, 1.0f, 1.0f,
	    1.0f,-1.0f,-1.0f,
	    1.0f, 1.0f,-1.0f,
	    1.0f,-1.0f,-1.0f,
	    1.0f, 1.0f, 1.0f,
	    1.0f,-1.0f, 1.0f,
	    1.0f, 1.0f, 1.0f,
	    1.0f, 1.0f,-1.0f,
	    -1.0f, 1.0f,-1.0f,
	    1.0f, 1.0f, 1.0f,
	    -1.0f, 1.0f,-1.0f,
	    -1.0f, 1.0f, 1.0f,
	    1.0f, 1.0f, 1.0f,
	    -1.0f, 1.0f, 1.0f,
	    1.0f,-1.0f, 1.0f
	};

	o->size = 12*3;	
	o->vertices = malloc(sizeof(point)*o->size); 
	o->normals = malloc(sizeof(point)*o->size);
	o->color = malloc(sizeof(float)*3*o->size);

	for(i = 0; i < o->size; i++)
	{
		o->vertices[i].x = vert[(i*3)] * size;
		o->vertices[i].y = vert[(i*3)+1] * size;
		o->vertices[i].z = vert[(i*3)+2] * size;

		o->normals[i].x = 0;
		o->normals[i].z = 0;
		o->normals[i].y = 1;

		o->color[(i*3)+0] = 0.1f;
		o->color[(i*3)+1] = 0.2f;
		o->color[(i*3)+2] = 0.3f;
	}

}

void obj_generateplane(obj *o, float size)
{
	int i;
	float vert[] = 
	{
		-1.0f,-0.1f,1.0f,
		1.0f, -0.1f,1.0f,
		-1.0f,-0.1f,-1.0f,

		1.0f, -0.1f,-1.0f,
		-1.0f,-0.1f,-1.0f,
		1.0f, -0.1f,1.0f
	};
	
	o->size = 2*3;	
	o->vertices = malloc(sizeof(point)*o->size); 
	o->normals = malloc(sizeof(point)*o->size);
	o->color = malloc(sizeof(float)*3*o->size);

	for(i = 0; i < o->size; i++)
	{
		o->vertices[i].x = vert[(i*3)+0] * size;
		o->vertices[i].y = vert[(i*3)+1] * size;
		o->vertices[i].z = vert[(i*3)+2] * size;

		o->normals[i].x = 0;
		o->normals[i].z = 0;
		o->normals[i].y = 1;

		o->color[(i*3)+0] = 0.1f;
		o->color[(i*3)+1] = 0.2f;
		o->color[(i*3)+2] = 0.3f;
	}



}

void obj_bindbuffers(obj *o, shader *s)
{
	glGenBuffers(1, &o->positionb);
	glBindBuffer(GL_ARRAY_BUFFER, o->positionb);
	glBufferData(GL_ARRAY_BUFFER, sizeof(point) * o->size, o->vertices, GL_STATIC_DRAW);

	glGenBuffers( 1, &o->colorb );
	glBindBuffer( GL_ARRAY_BUFFER, o->colorb );	
	glBufferData( GL_ARRAY_BUFFER, sizeof(float)*o->size*3, o->color, GL_STATIC_DRAW );

	glGenBuffers( 1, &o->normalb );
	glBindBuffer( GL_ARRAY_BUFFER, o->normalb );
	glBufferData( GL_ARRAY_BUFFER, sizeof(vector3)*o->size, o->normals, GL_STATIC_DRAW );

	glGenVertexArrays(1, &o->vao);
	glBindVertexArray(o->vao);
	glBindBuffer(GL_ARRAY_BUFFER, o->positionb);
    glVertexAttribPointer(0 , 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0 );

	glBindBuffer( GL_ARRAY_BUFFER, o->colorb );
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0 );
	glEnableVertexAttribArray(1 );

	glBindBuffer( GL_ARRAY_BUFFER, o->normalb );
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0 );
	glEnableVertexAttribArray(2);

	glBindVertexArray(0);

	o->s = s;
	o->modelml = glGetUniformLocation(s->s, "modelViewMatrix");
    o->projml = glGetUniformLocation(s->s, "projectionMatrix");
    o->normalml = glGetUniformLocation(s->s, "normalMatrix");
    o->cameraposml = glGetUniformLocation(s->s, "cameraPos");

}


void obj_render(obj *o, matrix4 proj, matrix4 model, matrix4 normal, point camerapos)
{
	float pos[] = {camerapos.x,camerapos.y,camerapos.z};
	glUseProgram(o->s->s);
	glUniformMatrix4fv(o->projml, 1, GL_FALSE, proj.values);
	glUniformMatrix4fv(o->modelml, 1, GL_FALSE, model.values);
	glUniformMatrix4fv(o->normalml, 1, GL_FALSE, normal.values);
	glUniformMatrix4fv(o->cameraposml, 1, GL_FALSE, pos);
	glBindVertexArray(o->vao);
	glDrawArrays(GL_TRIANGLES, 0, o->size);

	glBindVertexArray(0);
	glUseProgram(0);

}



