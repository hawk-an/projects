#ifndef CAMERA_H_
#define CAMERA_H_


#include <GL/glew.h>
#include <GL/glfw.h>

#include "matrix.h"
#include "point.h"
#include "vector.h" 

#define PIOVER180 0.0174532925f

typedef struct Camera camera;

struct Camera
{
	point eye;
	vector3 up;
	vector3 forward;
};

void camera_init(camera *c);
void camera_getright(camera *c, vector3 *right);
void camera_render(camera *c, matrix4 *model);
void camera_translate(camera *c, vector3 v, float s);
void camera_update(camera *c);
void camera_rotate(camera *c, vector3 v, float degree);

#endif
