#ifndef MATRIX_H_
#define MATRIX_H_

#include <math.h>
#include <string.h>
#include <stdio.h>
#include "vector.h"
#include "point.h"

#define degtorad(d) ((d / 180.0f) * M_PI)

typedef struct Matrix4 matrix4;
typedef struct Matrix3 matrix3;

struct Matrix4
{
	float values[16];
};

struct Matrix3{
	float values[9];
};

void matrix4_setidentity(matrix4 *mat);
void matrix4_setperspective(matrix4 *mat, float angle, float imageAspectRatio, float n, float f);
void matrix4_setfrustum(matrix4 *mat, float l, float r, float b, float t, float n, float f);
void matrix4_lookat(matrix4 *mat, point eye, point front, vector3 up);
void matrix4_print(matrix4 mat);
void matrix4_transpose(matrix4 *mat);
void matrix4_inverse(matrix4 *mat);

#endif
