#include "point.h"


void point_tovector3(point *p, vector3 *v)
{
	v->x = p->x;
	v->y = p->y;
	v->z = p->z;
	
}

void point_sumvector3(point *p, vector3 v)
{
	p->x += v.x;
	p->y += v.y;
	p->z += v.z;
}

void point_getvector3(vector3 *v, point p1, point p2)
{
	v->x = p1.x - p2.x;
	v->y = p1.y - p2.y;
	v->z = p1.z - p2.z;
}

