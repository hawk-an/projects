#ifndef OBJ_H_
#define OBJ_H_

#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>
#include "point.h"
#include "matrix.h"
#include "shader.h"

typedef struct object obj;

struct object
{
	point* vertices;
	GLuint positionb;

	vector3* normals;
	GLuint normalb;

	float* color;
	GLuint colorb;

	unsigned int size;
	GLuint vao;

	shader *s;
	GLuint projml;
	GLuint modelml;
	GLuint normalml;
	GLuint cameraposml;
//	unsigned int* indices;
};

void obj_generatecube(obj *o, float size);
void obj_generateplane(obj *o, float size);
void obj_bindbuffers(obj *o, shader *s);
void obj_render(obj *o, matrix4 proj, matrix4 model, matrix4 normal, point camerapos);

#endif
