#ifndef POINT_H_
#define POINT_H_

#include "vector.h"

typedef struct Point3 point;

struct Point3
{
	float x;
	float y;
	float z;
};

void point_tovector3(point *p, vector3 *v);
void point_sumvector3(point *p, vector3 v);
void point_getvector3(vector3 *v, point p1, point p2);

#endif
