#include "shader.h"

void shader_printshaderinfo(GLuint obj)
{
    int infologLength = 0;
    int charsWritten  = 0;
    char *infoLog;
 
    glGetShaderiv(obj, GL_INFO_LOG_LENGTH,&infologLength);
 
    if (infologLength > 1)
    {
        infoLog = (char *)malloc(infologLength);
        glGetShaderInfoLog(obj, infologLength, &charsWritten, infoLog);
        printf("%s\n",infoLog);
        free(infoLog);
    }
}

void shader_printprograminfo(GLuint obj)
{
    int infologLength = 0;
    int charsWritten  = 0;
    char *infoLog;
 
    glGetProgramiv(obj, GL_INFO_LOG_LENGTH,&infologLength);
 
    if (infologLength > 1)
    {
        infoLog = (char *)malloc(infologLength);
        glGetProgramInfoLog
			(obj, infologLength, &charsWritten, infoLog);
        printf("%s\n",infoLog);
        free(infoLog);
		return;
    }
	fprintf(stdout, "OK!\n");
	fflush(stdout);
}

void shader_update(shader *shade)
{
	struct timeval tv;
	char buffer[EVENT_BUF_LEN];
	fd_set rfds;
	int retval,length;

	tv.tv_sec = 0;
	tv.tv_usec = 0;

	FD_ZERO( &rfds);
	FD_SET(shade->fd, &rfds);
	
	retval = select(shade->fd +1, &rfds, NULL, NULL, &tv);

	if(retval && FD_ISSET(shade->fd, &rfds)){
		int i = 0;
		length = read( shade->fd, buffer, EVENT_BUF_LEN );
		while (i < length)
		{
			struct inotify_event *event =
				(struct inotify_event*) &buffer[ i ];
			if (event->len > 0 && 
				(event->mask & IN_MODIFY))
			{
				if((0 == strcmp(event->name, shade->vertfile)) ||
					(0 == strcmp(event->name, shade->fragfile)))
				{
					printf( "File %s changed, recompiling...\n"
						,event->name );
					glUseProgram(0);
					glDeleteProgram(shade->s);

					shader_load(shade);
				}
			}
			i += EVENT_SIZE + event->len;
		}
	}

}

void shader_init(shader *shade, char *folder, char *name)
{
	char *vert = ".vert";
	char *frag = ".frag";
	
	shade->vertfile = (char*) malloc((strlen(name) + 6)*sizeof(char));
	shade->fragfile = (char*) malloc((strlen(name) + 6)*sizeof(char));
	shade->folder = (char*) malloc(strlen(folder) * sizeof(char));

	memcpy(shade->folder, folder, strlen(folder) * sizeof(char));
	
	memcpy(shade->vertfile, name, strlen(name) * sizeof(char));
	memcpy(shade->fragfile, name, strlen(name) * sizeof(char));
	
	memcpy(shade->vertfile + strlen(name), vert, sizeof(vert));
	memcpy(shade->fragfile + strlen(name), frag, sizeof(frag));

	
	shade->fd = inotify_init();
 	shade->wd = inotify_add_watch(
		shade->fd, folder, IN_MODIFY | IN_CREATE | IN_DELETE );
 	

	shader_load(shade);
}

void shader_load(shader *shade)
{
    char *vs = NULL,*fs = NULL;
	char vert[(strlen(shade->vertfile)
		+ strlen(shade->folder) + 1) * sizeof(char)];
	char frag[(strlen(shade->fragfile)
		+ strlen(shade->folder) + 1) * sizeof(char)];
    GLuint v,f;
	
	printf("Loading: %s , %s\n",
		shade->vertfile, shade->fragfile);

	memcpy(vert, shade->folder,
		(strlen(shade->folder) + 1) * sizeof(char));
	memcpy(frag, shade->folder,
		(strlen(shade->folder) + 1) * sizeof(char));


	memcpy(vert + strlen(vert), 
		shade->vertfile, (strlen(shade->vertfile) + 1) * sizeof(char));
	memcpy(frag + strlen(frag), 
		shade->fragfile, (strlen(shade->fragfile) + 1) * sizeof(char));

	
	/// Load shader
    v = glCreateShader(GL_VERTEX_SHADER);
    f = glCreateShader(GL_FRAGMENT_SHADER);
 
    vs = textfile_read(vert);
    fs = textfile_read(frag);

	if(vs == NULL || fs == NULL || v == 0 || f == 0)
	{
		fprintf(stderr, "Failed to read shaders\n");
		return;
	}
    const char * vv = vs;
    const char * ff = fs;
 
    glShaderSource(v, 1, &vv,NULL);
    glShaderSource(f, 1, &ff,NULL);
 
    free(vs);free(fs);
 
    glCompileShader(v);
    glCompileShader(f);
 
    shader_printshaderinfo(v);
    shader_printshaderinfo(f);
 
    shade->s = glCreateProgram();
    glAttachShader(shade->s,v);
    glAttachShader(shade->s,f);

	glDeleteShader( v);
	glDeleteShader( f);


	glBindAttribLocation(shade->s, 0, "position"); 
	glBindAttribLocation(shade->s, 1, "color");
	glBindAttribLocation(shade->s, 2, "normal");
	glBindFragDataLocation(shade->s, 0, "fragmentColor");

//    glBindFragDataLocation(*p, 0, "outputF");
    glLinkProgram(shade->s);
    shader_printprograminfo(shade->s);
 
//    vertexLoc = glGetAttribLocation(p,"position");
//    colorLoc = glGetAttribLocation(p, "color"); 
 
//    projMatrixLoc = glGetUniformLocation(p, "projMatrix");
//    viewMatrixLoc = glGetUniformLocation(p, "viewMatrix");

}

