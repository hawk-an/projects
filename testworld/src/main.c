#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glew.h>
#include <GL/glfw.h>

#include "camera.h"
#include "shader.h"
#include "obj.h"

#include "matrix.h"

#define false 0
#define true 1

int width = 1366;
int height = 768;
char* title = "testworld";
int redBits = 8;
int greenBits = 8;
int blueBits = 8;
int alphaBits = 8;
int depthBits = 24;
int stencilBits = 0;
int aa = 4;
int mode = GLFW_WINDOW;

matrix4 projMatrix;
matrix4 viewMatrix;
matrix4 normalMatrix;
GLdouble fov = 45;
GLdouble ratio;
GLdouble nearDist = 0.1;
GLdouble farDist = 100;
GLuint positionBuffer;
GLuint colorBuffer; 
GLuint vao;
//GLuint shader;

obj cube;
obj plane;
camera cam;
shader shade;


int main(int argc, char *argv[]) {

	ratio = width/height;

	obj_generatecube(&cube, 1.0f);
	obj_generateplane(&plane, 5.0f);
	camera_init(&cam);


	if(!glfwInit()) {
		fprintf(stderr, "Failed to initialize GLFW!\n");
		return 1;
	}

	glfwOpenWindowHint(GLFW_FSAA_SAMPLES, aa);
	if((glfwOpenWindow(
		width, height, redBits, greenBits, 
		blueBits, alphaBits, depthBits, 
		stencilBits, mode))!= GL_TRUE)
	{
		glfwTerminate();
		exit(1);
	}
	glfwSetWindowTitle(title);

	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		fprintf(stderr, "Failed to initialize GLEW!\n");
		exit(1);
	}
	if (!GLEW_VERSION_2_1)
	{
		fprintf(stderr, "GLEW 2.1 not supported!\n");
		exit(1);
	}

	
	glViewport(0, 0, width, height);


    glfwSwapInterval(1);

	shader_init(&shade, "shader/", "shader");
	
	
//	shader_load(&shade.shade,"shader/shader.vert","shader/shader.frag");
	obj_bindbuffers(&cube, &shade);
	obj_bindbuffers(&plane, &shade);




//	glDisable(GL_DEPTH_TEST);
//	glDisable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);
    glEnable(GL_NORMALIZE);

	glDepthMask(GL_TRUE);

	glEnable(GL_BLEND);

	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
 
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
    
    glShadeModel(GL_SMOOTH);

	matrix4_setidentity(&projMatrix);
	matrix4_setidentity(&viewMatrix);
	

	matrix4_setperspective(&projMatrix,fov, ratio, nearDist, farDist);
	while(!glfwGetKey(GLFW_KEY_ESC))
	{

		shader_update(&shade);
		camera_update(&cam);
		camera_render(&cam, &viewMatrix);
		
		memcpy(normalMatrix.values,
			viewMatrix.values, sizeof(float)*16);
		matrix4_transpose(&normalMatrix);
		matrix4_inverse(&normalMatrix);


		glClearColor(0.1, 0.1, 0.1, 1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		
		obj_render(&cube,
			projMatrix, viewMatrix, normalMatrix, cam.eye );
		obj_render(&plane,
			projMatrix, viewMatrix, normalMatrix, cam.eye );
	
   		glfwSwapBuffers(); 
	}
	
	return 0;

}
