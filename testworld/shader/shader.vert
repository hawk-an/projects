#version 130


 
in vec3 position;
in vec3 color;
in vec3 normal;

out vec3 fcolor;
out vec3 viewSpaceNormal;
out vec3 viewSpacePosition;
out vec3 viewSpaceCameraPos;
out vec3 viewSpaceLightPos;

uniform mat4 modelViewMatrix, projectionMatrix, normalMatrix;
uniform vec3 cameraPos;


void main()
{
    viewSpaceLightPos = (modelViewMatrix * vec4(0.0,10.0,10.0, 1.0)).xyz;
    viewSpaceCameraPos = (modelViewMatrix * vec4(cameraPos,1.0)).xyz;
    viewSpaceNormal = (normalMatrix * vec4(normal,0.0)).xyz;
    viewSpacePosition = (modelViewMatrix * vec4(position, 1.0)).xyz;
    
    fcolor = color;
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1);
}








