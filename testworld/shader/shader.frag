#version 130
 
in vec3 fcolor;
in vec3 viewSpaceNormal;
in vec3 viewSpacePosition;
in vec3 viewSpaceCameraPos;
in vec3 viewSpaceLightPos;

//out vec4 outputF;
precision highp float;

out vec4 fragmentColor;


void main()
{
    vec3 directionFromEye =
        normalize(viewSpacePosition - viewSpaceCameraPos);
    vec3 directionToLight =
        normalize(viewSpaceLightPos - viewSpacePosition) ;
    vec3 normal = normalize(viewSpaceNormal);
    
    vec3 h = normalize(directionToLight - directionFromEye);
    float specular = pow(max(0, dot(h, normal)), 0.8);
    
    float diffuse = max(0, dot(directionToLight, normal));
    
    vec3 shading = diffuse * fcolor;
    //normalize(shading);
    fragmentColor = vec4(shading,1);
}

